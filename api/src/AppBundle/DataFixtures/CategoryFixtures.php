<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Yaml\Yaml As Yaml;

class CategoryFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $data = Yaml::parse(file_get_contents(__DIR__ .'/Data/category.yml'));
        foreach ($data as $value){
            $data = new Category();
            $data->setIdentifier($value['identifier']);
            $data->setTitle($value['title']);
            $data->setIcon($value['icon']);
            $data->setDescription($value['description']);
            $data->setActive($value['active']);
            if(array_key_exists('parent', $value)){
              $data->setParent($this->getReference($value['parent']));
            }
            $manager->persist($data);
            $this->addReference($value['reference'], $data);
        }
        $manager->flush();
    }

}