<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Store;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Yaml\Yaml As Yaml;

class StoreFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $data = Yaml::parse(file_get_contents(__DIR__ .'/Data/store.yml'));
        foreach ($data as $value){
          $store = new Store();
          $store->setIdentifier($value['identifier']);
          $store->setName($value['name']);
          $store->setDescription($value['description']);
          $store->setCreatedAt( new \DateTime());
          $store->setUpdateAt( new \DateTime());
          $store->setActive(true);
          $store->addCategory($this->getReference($value['category']));
          $store->setAddress($this->getReference($value['address']));
          //$store->setImage();
          $store->setUser($this->getReference($value['user']));
          $manager->persist($store);
          $this->addReference($value['reference'], $store);
        }
         $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            PostalAddressFixtures::class
        );
    }

}