<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\PostalAddress;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Yaml\Yaml As Yaml;

class PostalAddressFixtures extends Fixture
{

  public function load(ObjectManager $manager)
  {
    $data = Yaml::parse(file_get_contents(__DIR__ .'/Data/postalAddress.yml'));
    foreach ($data as $value){
      $object = new PostalAddress();
      $object->setName($value['name']);
      $object->setAddressCountry($value['addressCountry']);
      $object->setAddressLocality($value['addressLocality']);
      $object->setAddressRegion($value['addressRegion']);
      $object->setPostalCode($value['PostalCode']);
      $object->setStreetAddress($value['streetAddress']);
      $object->setCoordinate($value['coordinate']);
      $manager->persist($object);
      $this->addReference($value['reference'], $object);
    }
     $manager->flush();
  }
 
}