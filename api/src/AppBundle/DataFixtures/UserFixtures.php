<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Yaml\Yaml As Yaml;

class UserFixtures extends Fixture
{

  public function load(ObjectManager $manager)
  {
    $data = Yaml::parse(file_get_contents(__DIR__ .'/Data/user.yml'));
    foreach ($data as $value){
      $user = new User();
      $user->setLastname($value['lastName']);
      $user->setUserName($value['userName']);
      $user->setEmail($value['email']);
      $user->setPlainPassword($value['plainPassword']);
      $user->addRole($value['plainPassword']);
      $user->setEnabled($value['enabled']);
      $manager->persist($user);
      $this->addReference($value['reference'], $user);
    }
     $manager->flush();
  }
 
}