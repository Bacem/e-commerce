<?php

/*
 * This file is part of the Ecommerce Platforme package.
 *
 * (c) SAKLI Bacem <sakli.bacem@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace AppBundle\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Any offered product or service. For example: a pair of shoes; a concert ticket; the rental of a car; a haircut; or an episode of a TV show streamed online.
 *
 * @see http://schema.org/Product Documentation on Schema.org
 *
 * @author SAKLI Bacem <sakli.bacem@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(iri="http://schema.org/Product")
 */
class Product
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string|null The identifier property represents any kind of identifier for any kind of \[\[Thing\]\], such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides dedicated properties for representing many of these, either as textual strings or as URL (URI) links. See \[background notes\](/docs/datamodel.html#identifierBg) for more details.
     *
     * @ORM\Column(type="string" , nullable = false, unique = true)
     * @ApiProperty(iri="http://schema.org/identifier")
     * @Assert\Url
     */
    private $identifier;

    /**
     * @var string the name of the item
     *
     * @ORM\Column(type="text")
     * @ApiProperty(iri="http://schema.org/name")
     * @Assert\NotNull
     */
    private $name;

    /**
     * @var string a description of the item
     *
     * @ORM\Column(type="text")
     * @ApiProperty(iri="http://schema.org/description")
     * @Assert\NotNull
     */
    private $description;
    
    /**
     * @var json array a description of the item
     *
     * @ORM\Column(type="json_array")
     */
    private $property;    

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="date")
     * @Assert\Date
     * @Assert\NotNull
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="date")
     * @Assert\Date
     * @Assert\NotNull
     */
    private $updateAt;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     * @Assert\NotNull
     */
    private $active;

    /**
     * @var Brand the brand(s) associated with a product or service, or the brand(s) maintained by an organization or business person
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Brand")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(iri="http://schema.org/brand")
     * @Assert\NotNull
     */
    private $brand;

    /**
     * @var Image|null An image of the item. This can be a \[\[URL\]\] or a fully described \[\[ImageObject\]\].
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Image")
     * @ApiProperty(iri="http://schema.org/image")
     */
    private $image;

    /**
     * @var Offer|null
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Offer")
     */
    private $offer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setIdentifier(?string $identifier): void
    {
        $this->identifier = $identifier;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): void
    {
        $this->updateAt = $updateAt;
    }

    public function getUpdateAt(): \DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function setBrand(Brand $brand): void
    {
        $this->brand = $brand;
    }

    public function getBrand(): Brand
    {
        return $this->brand;
    }

    public function setImage(?Image $image): void
    {
        $this->image = $image;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setOffer(?Offer $offer): void
    {
        $this->offer = $offer;
    }

    public function getOffer(): ?Offer
    {
        return $this->offer;
    }
}
