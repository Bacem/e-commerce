<?php

/*
 * This file is part of the Ecommerce Platforme package.
 *
 * (c) SAKLI Bacem <sakli.bacem@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace AppBundle\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * The mailing address.
 *
 * @see http://schema.org/PostalAddress Documentation on Schema.org
 *
 * @author SAKLI Bacem <sakli.bacem@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(iri="http://schema.org/PostalAddress")
 */
class PostalAddress
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string the name of the item
     *
     * @ORM\Column(type="text")
     * @ApiProperty(iri="http://schema.org/name")
     * @Assert\NotNull
     */
    private $name;

    /**
     * @var string The country. For example, USA. You can also provide the two-letter \[ISO 3166-1 alpha-2 country code\](http://en.wikipedia.org/wiki/ISO\_3166-1).
     *
     * @ORM\Column(type="text")
     * @ApiProperty(iri="http://schema.org/addressCountry")
     * @Assert\NotNull
     */
    private $addressCountry;

    /**
     * @var string The locality. For example, Mountain View.
     *
     * @ORM\Column(type="text")
     * @ApiProperty(iri="http://schema.org/addressLocality")
     * @Assert\NotNull
     */
    private $addressLocality;

    /**
     * @var string The region. For example, CA.
     *
     * @ORM\Column(type="text")
     * @ApiProperty(iri="http://schema.org/addressRegion")
     * @Assert\NotNull
     */
    private $addressRegion;

    /**
     * @var string The postal code. For example, 94043.
     *
     * @ORM\Column(type="text")
     * @ApiProperty(iri="http://schema.org/postalCode")
     * @Assert\NotNull
     */
    private $postalCode;

    /**
     * @var string The street address. For example, 1600 Amphitheatre Pkwy.
     *
     * @ORM\Column(type="text")
     * @ApiProperty(iri="http://schema.org/streetAddress")
     * @Assert\NotNull
     */
    private $streetAddress;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\NotNull
     */
    private $coordinate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setAddressCountry(string $addressCountry): void
    {
        $this->addressCountry = $addressCountry;
    }

    public function getAddressCountry(): string
    {
        return $this->addressCountry;
    }

    public function setAddressLocality(string $addressLocality): void
    {
        $this->addressLocality = $addressLocality;
    }

    public function getAddressLocality(): string
    {
        return $this->addressLocality;
    }

    public function setAddressRegion(string $addressRegion): void
    {
        $this->addressRegion = $addressRegion;
    }

    public function getAddressRegion(): string
    {
        return $this->addressRegion;
    }

    public function setPostalCode(string $postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    public function setStreetAddress(string $streetAddress): void
    {
        $this->streetAddress = $streetAddress;
    }

    public function getStreetAddress(): string
    {
        return $this->streetAddress;
    }

    public function setCoordinate(string $coordinate): void
    {
        $this->coordinate = $coordinate;
    }

    public function getCoordinate(): string
    {
        return $this->coordinate;
    }
}
