import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {
    MatButtonModule,
    MatDialogModule, MatIconModule, MatTabsModule, MatToolbarModule, MatInputModule
} from "@angular/material";
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatIconModule, MatDialogModule, MatToolbarModule, MatButtonModule, MatTabsModule, MatInputModule
  ],
  declarations: [
      HeaderComponent,
      LoginComponent
  ],
  exports: [
      HeaderComponent,
      LoginComponent
  ],
  entryComponents: [
      LoginComponent
  ]
})
export class CoreModule { }
