import { Component, OnInit } from '@angular/core';
import {LoginComponent} from "../login/login.component";
import {VERSION, MatDialog, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    fileNameDialogRef: MatDialogRef<LoginComponent>;

    constructor(private dialog: MatDialog) {}

    ngOnInit() {
    }

    openAddFileDialog() {
        this.fileNameDialogRef = this.dialog.open(LoginComponent);
    }

}
