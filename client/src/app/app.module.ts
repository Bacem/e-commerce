import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {
    MatToolbarModule, MatSidenavModule, MatButtonModule, MatIconModule, MatListModule, MatGridListModule,
    MatMenuModule
} from '@angular/material';

import { CoreModule } from './core/core.module';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule,
    MatToolbarModule, MatSidenavModule, MatButtonModule, MatIconModule, MatListModule, MatGridListModule, MatMenuModule,
    CoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
