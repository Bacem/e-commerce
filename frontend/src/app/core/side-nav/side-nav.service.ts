import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SideNavService {
  constructor(
      private http: HttpClient,
  ) { }

  getCategories(): Observable<any> {
       return this.http.get('http://localhost/app_dev.php/categories');
  }
}
