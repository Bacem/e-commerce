import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SideNavService } from './side-nav.service';


@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {
    categories: any;
  constructor(
      private http: HttpClient,
      private sideNavService: SideNavService
  ) { }

  ngOnInit() {
      this.sideNavService.getCategories().subscribe(categories => {
          this.categories = categories["hydra:member"];
      });
  }
}
