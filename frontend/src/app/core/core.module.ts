import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClarityModule } from 'clarity-angular';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { NavHeaderComponent } from './nav-header/nav-header.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { SideNavService } from './side-nav/side-nav.service';
import { SideNavProfileComponent } from './side-nav-profile/side-nav-profile.component';
import { SubNavComponent } from './sub-nav/sub-nav.component';
import {SubNavService} from "./sub-nav/sub-nav.service";

@NgModule({
  imports: [
      CommonModule,
      ClarityModule,
      HttpClientModule,
      RouterModule
  ],
  declarations: [
      NavHeaderComponent,
      SideNavComponent,
      SideNavProfileComponent,
      SubNavComponent
  ],
   exports: [
      NavHeaderComponent,
      SideNavComponent,
      SideNavProfileComponent,
      SubNavComponent
   ],
    providers: [
        SideNavService,
        SubNavService
    ]
})
export class CoreModule { }
