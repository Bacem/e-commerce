import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class SubNavService {
    constructor(
        private http: HttpClient,
    ) { }

    getThemes(): Observable<any> {
        return this.http.get('http://localhost:3000/themes');
    }
}
