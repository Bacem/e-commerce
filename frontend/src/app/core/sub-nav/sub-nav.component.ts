import { Component, OnInit } from '@angular/core';
import {SubNavService} from '../sub-nav/sub-nav.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-sub-nav',
  templateUrl: './sub-nav.component.html',
  styleUrls: ['./sub-nav.component.css']
})
export class SubNavComponent implements OnInit {

    themes: any;
    constructor(
        private http: HttpClient,
        private subNavService: SubNavService
    ) { }

    ngOnInit() {
        this.subNavService.getThemes().subscribe(themes => {
            this.themes = themes;
        });
    }

}
