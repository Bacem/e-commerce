import { Component, OnInit } from '@angular/core';
import {CookieService} from 'ngx-cookie';
import {Router} from '@angular/router';
import {AppService} from '../../app.service';

@Component({
  selector: 'app-side-nav-profile',
  templateUrl: './side-nav-profile.component.html',
  styleUrls: ['./side-nav-profile.component.css']
})
export class SideNavProfileComponent implements OnInit {
  profile: any;
  constructor(
      private router: Router,
      private cookieService: CookieService,
      private appService: AppService
  ) { }

  ngOnInit() {
    this.profile = this.cookieService.getObject('profile');
  }
  logout() {
      this.cookieService.removeAll();
      this.appService.setSpace('marketPlace');
      this.router.navigateByUrl('user/login');
  }
}
