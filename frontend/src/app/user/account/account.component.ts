import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie';
import { AppService } from '../../app.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  profile: any;
  typeAlert: string;
  textAlert: string;
  constructor(
      private cookieService: CookieService,
      private appService: AppService
  ) { }

  ngOnInit() {
    this.appService.setSpace('profile');
    this.typeAlert = 'info';
    this.profile = this.cookieService.getObject('profile');
    this.textAlert = 'you want to create your shop just click <a>New shop</a>';
  }
}
