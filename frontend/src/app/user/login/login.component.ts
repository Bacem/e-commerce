import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie';
import {AppService} from '../../app.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  typeAlert: string;
  textText: string;
  profile: any;
  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private loginService: LoginService,
      private cookieService: CookieService,
      private appService: AppService
  ) { }

  ngOnInit() {
      this.profile = this.cookieService.getObject('profile');
      if (this.profile) {
          this.router.navigateByUrl('user/account');
      }else{
          this.appService.setSpace('login');
      }
      /*when active account*/
      this.route.params.subscribe((params) => {
          if ( params['token'] !== undefined ) {
              this.loginService.active(params['token']).subscribe(
                  dataactive => {
                      this.typeAlert = 'success';
                      this.textText = dataactive;
                  },
                  errorLogin => {
                      this.typeAlert = 'danger';
                      this.textText = errorLogin.error.message;
                  }
              );
          }
      });
  }

  onSubmit(loginForm) {
      this.loginService.login(loginForm.value).subscribe(
          dataLogin => {
              this.cookieService.putObject('profile', dataLogin);
              this.router.navigateByUrl('user/account');
          },
          errorLogin => {
              this.typeAlert = 'danger';
              this.textText = errorLogin.error.message;
          }
      );
  }
}
