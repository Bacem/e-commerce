import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarketplaceRoutingModule } from './marketplace-routing.module';
import { HomeComponent } from './home/home.component';

@NgModule({
  imports: [
    CommonModule,
    MarketplaceRoutingModule
  ],
  declarations: [
      HomeComponent
  ]
})
export class MarketplaceModule { }
