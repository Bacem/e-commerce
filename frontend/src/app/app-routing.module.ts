import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: 'user',
        loadChildren: 'app/user/user.module#UserModule',
    },
    {
        path: 'marketplace',
        loadChildren: 'app/marketplace/marketplace.module#MarketplaceModule'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
