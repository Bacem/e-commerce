import { Component, OnChanges } from '@angular/core';
import {AppService} from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ AppService ]
})
export class AppComponent {
    space: string;
    constructor( private appService: AppService ) {
        this.space = 'marketPlace'
        appService.spaceObservable$.subscribe(
            space => {
                this.space = space;
            });
    }
}
